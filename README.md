### LiberaForms is Not Google forms
Project page https://liberaforms.org

Thanks for this friendly forms software!

This repo contains Dockerfiles and docker-compose.yml recipes to run liberaforms and its db, mongodb.


Two dockers containers are going to raise:
*  liberaforms, process run by user liberaforms with uid 2000, volume at /opt/liberaforms/config.
*  mongodb, process run by user mongdb with uid 2100, volume at /opt/liberaforms/mongodb-data

### Install 
```
git clone https://git.sindominio.net/pebles/liberaforms-docker.git /opt/liberaforms
```

### Create volumes.
Create the config directory with gid 2000:
```
mkdir  /opt/liberaforms/config  
chown root:2000 /opt/liberaforms/config  
chmod 750 /opt/liberaforms/config
```

Create the data persistent directory with uid 2100 for owner:
```
mkdir  /opt/liberaforms/mongodb-data  
chown 2100 /opt/liberaforms/mongodb-data  
chmod 700 /opt/liberaforms/mongodb-data  
```

Users liberaforms and mongdb are created on the fly (and destroyed) by Dockerfiles, so don't worry about that.  
But if you want to be sured there's no collision with host users ids, you could create such users on host:
```
useradd -rm -d /opt/liberaforms -s /usr/sbin/nologin -u 2000 liberaforms  
useradd -rm -d /opt/liberaforms/mongodb  -s /usr/sbin/nologin -u 2100 mongodb  
```
Collisions can be a security issue, so the container process run with the privileges of that account id.  
Of course, you can use system reserved uids; check your host `/etc/password` file for choosing a not being used number under 1000 (on debian).  

### Configure files
Copy examples configs to files:

* **gunicorn.py:**  
  Change interfaces and port, 5000 by default. Use '0.0.0.0' as interface if server is behind proxy. 
  The user here is the same that runs the container, `liberaforms`.  

* **config.cfg:**  
  Set a secret:
  Configure email addresses for admins.  
  The db field is already configured to point to 'mongodb' host, that docker-compose will raise with this host identifier. Notice you can force the hostname at docker-compose.yml with `host: myhostname`.

### Run  
```
docker-compose build  
docker-compose up -d  
```

### Use  
First time you log in, try to recover the password. Then fill your admin email (as in config.cfg), revover. A registration form will load for your superadmin.
You can use these method for any email in config file, or add users by invitation url. On second case you can grant admin role. 
All admins, invited or defined at config.cfg file, just have read permissions on forms created by others. Instead they can disable the form or change de owner.



## Update
For updating just build the image again (maybe cron). You config and db is persistent, so no fear!

Enjoy!


