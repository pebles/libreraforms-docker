pythonpath = '/usr/bin/'
command = '/usr/local/bin/gunicorn'
#If your nginx proxy is on another machine, try 0.0.0.0 instead
bind = '0.0.0.0:5000'
workers = 3
user = 'liberaforms'

